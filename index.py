from flask import Flask
from flask_restful import Resource, Api
from bs4 import BeautifulSoup
import urllib2
import json
from flask import request
import re

app = Flask(__name__)
api = Api(app)

class Test(Resource):
    def get(self):
    	url = request.args.get('url')
    	searched_word = request.args.get('word')
    	page=urllib2.urlopen(url)
    	
    	soup = BeautifulSoup(page.read(), 'html.parser')

    	results = soup.body.find_all(string=re.compile('.*{0}.*'.format(searched_word)), recursive=True)
		
    	return {searched_word: len(results)}


api.add_resource(Test, '/')

if __name__ == '__main__':
    app.run(debug=True)


